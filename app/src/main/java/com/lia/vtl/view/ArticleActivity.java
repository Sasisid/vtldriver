package com.lia.vtl.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lia.vtl.R;
import com.lia.vtl.bo.Article;
import com.lia.vtl.utility.Session;
import com.lia.vtl.utility.StaticInfo;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ArticleActivity extends AppCompatActivity {
    ImageView img;
    TextView arttext;
    Article[] listdata;
   ArticleAdapter adapter;
    RecyclerView recyclerView;
    private Session session;
    private ProgressBar progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        session = new Session(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        img=(ImageView)findViewById(R.id.img);
        arttext=(TextView) findViewById(R.id.arttext);
        progress=findViewById(R.id.aprogress);
        arttext.setText(StaticInfo.article.getAdescription());
        Glide.with(this)
                .load(StaticInfo.LocalImgPath+StaticInfo.article.getAimage()).placeholder(R.drawable.defaultimg)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progress.setVisibility(View.GONE);
                        img.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progress.setVisibility(View.GONE);
                        img.setVisibility(View.VISIBLE);
                        return false;
                    }
                })
                .into(img);
//        Picasso.get().load("https://www.vtlpl.com/app/upload/"+StaticInfo.article.getAimage()).into(img);


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerarticle);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        Article[] objects=new Article[StaticInfo.listArticle.size()];

        for(int i=0;i<StaticInfo.listArticle.size();i++){
            objects[i]=StaticInfo.listArticle.get(i);
        }


        adapter = new ArticleAdapter(objects);

        recyclerView.setAdapter(adapter);
    }

    /*** Web Servcice Client  ***/

    public static String GET(String url){
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }


    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
    public  class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ViewHolder>{
        private String[] mDataset;
        private Article[] listdata;
        //    listdata
        private String imgPath="";
        //    session = new Session(this);
        // RecyclerView recyclerView;
        public ArticleAdapter(Article[] listdata) {
            this.listdata = listdata;
        }
        @Override
        public ArticleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem= layoutInflater.inflate(R.layout.article_layout, parent, false);
            ArticleAdapter.ViewHolder viewHolder = new ArticleAdapter.ViewHolder(listItem);
            return viewHolder;
        }



        // Provide a suitable constructor (depends on the kind of dataset)
        public ArticleAdapter(String[] myDataset) {
            mDataset = myDataset;
        }

        @Override
        public  void onBindViewHolder(ArticleAdapter.ViewHolder holder, int position) {
            final Article myListData = listdata[position];
//        holder.textView.setText(listdata[position].getDescription());
            Glide.with(ArticleActivity.this)
                    .load(StaticInfo.LocalImgPath+myListData.getAimage())
                    .into(holder.imageView);
//            Picasso.get().load("https://www.vtlpl.com/app/upload/"+myListData.getAimage()).into(holder.imageView);
            Log.d("url",StaticInfo.LocalImgPath+myListData.getAimage());
            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new HttpAsyncArticleTask().execute(StaticInfo.getArticleByIdUrl(myListData.getId()));
//                    Toast.makeText(view.getContext(),"click on item: "+myListData.getId(),Toast.LENGTH_LONG).show();
                }
            });
        }


        @Override
        public int getItemCount() {
            return listdata.length;
        }

        public  class ViewHolder extends RecyclerView.ViewHolder {
            public ImageView imageView;
            //        public TextView textView;
            public LinearLayout relativeLayout;
            public ViewHolder(View itemView) {
                super(itemView);


                this.imageView = (ImageView) itemView.findViewById(R.id.articleimg);
//            this.textView = (TextView) itemView.findViewById(R.id.textView);
                relativeLayout = (LinearLayout)itemView.findViewById(R.id.articlelayout);
            }
        }


//
//    public boolean isConnected(){
//        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
//        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
//        if (networkInfo != null && networkInfo.isConnected())
//            return true;
//        else
//            return false;
//    }

        private class HttpAsyncArticleTask extends AsyncTask<String, Void, String> {
            @Override
            protected String doInBackground(String... urls) {
                return GET(urls[0]);
            }
            @Override
            protected void onPostExecute(String result) {
                // Toast.makeText(getBaseContext(), "Received:"+result, Toast.LENGTH_LONG).show();
                try {
                    if(result.equals("0")){
//                    Toast.makeText(getBaseContext(), "Invalid Credentials 1", Toast.LENGTH_LONG).show();
//                    popupWindow.dismiss();
                    }else if(result.indexOf("success::")!=-1){
                        Log.d("received",result);
                        result=result.replace("success::","");
                        Log.d("received1",result);

//                    Toast.makeText(getBaseContext(), "Authentication successfully", Toast.LENGTH_LONG).show();
//                    Intent intent = new Intent(LoginPage.this, MainActivity.class);
//                    startActivity(intent);
                        ObjectMapper mapper = new ObjectMapper();
                        ArrayList<Article> driverlist= mapper.readValue(result,new TypeReference<ArrayList<Article>>() {});
                        StaticInfo.article=  driverlist.get(0);
//                    StaticInfo.driver=techList.get(0);
                        //session.logoutUser();
                        // Toast.makeText(getBaseContext(), "lly"+StaticInfo.technician, Toast.LENGTH_LONG).show();
//                        StaticInfo.userId= StaticInfo.driver.getId();
//                        session.setUserId(StaticInfo.driver.getId());
                        session.setarticle(""+result);
                        Intent intent = new Intent(getBaseContext(), ArticleActivity.class);
                        startActivity(intent);
                        //session.createLoginSession("userId",""+StaticInfo.userId) ;
//                    Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
//                    ClientService cs=new ClientService(getBaseContext());
//                    cs.callservice();
//                    new ListLeadsHttpAsyncTask().execute(StaticInfo.getListLeadsUrl(StaticInfo.userId));
//                    new HttpAsyncArticleTask().execute(StaticInfo.getUpdateRegisterUrl(StaticInfo.driver.getId(),nameStr,StaticInfo.driver.getUserImg(), dobStr,StaticInfo.driver.getGender(),StaticInfo.driver.getMaritalStatus(),StaticInfo.driver.getAddress(),districtStr,stateStr,countrystr,StaticInfo.driver.getAdhar(),StaticInfo.driver.getAadhaarImg(),StaticInfo.driver.getPancard(),StaticInfo.driver.getLicenseNo(),StaticInfo.driver.getLicenceFrontImg(),StaticInfo.driver.getLicenceBackImg(),StaticInfo.driver.getLicenseExpiryDate(),StaticInfo.driver.getBatch(),StaticInfo.driver.getBatchExpiry(),StaticInfo.driver.getDrivingExperience(),StaticInfo.driver.getVehicleHandling(),StaticInfo.driver.getEmerContNo(),StaticInfo.driver.getInsurance(),StaticInfo.driver.getEmployeeType(),StaticInfo.driver.getReferPersonName(),StaticInfo.driver.getReferContactNo(),StaticInfo.driver.getBankAcNo(),StaticInfo.driver.getPassbookImg(),StaticInfo.driver.getAdvancePayment(),StaticInfo.driver.getSalaryCycle(),StaticInfo.driver.getSplitPayment(),"D",pincodeStr));
//                        new com.lia.vtl.adapter.ArticleAdapter.HttpAsyncArticleTask().execute(StaticInfo.getArticleUrl());



                    }
//                Toast.makeText(DashboardActivity.this, "Received:"+result, Toast.LENGTH_LONG).show();
//
//                ObjectMapper mapper = new ObjectMapper();
//                ArrayList<Article> listArticle= mapper.readValue(result,new TypeReference<ArrayList<Article>>() {});





                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}