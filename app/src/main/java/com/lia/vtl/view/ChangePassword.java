package com.lia.vtl.view;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lia.vtl.R;
import com.lia.vtl.utility.StaticInfo;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ChangePassword extends AppCompatActivity {
    EditText curpswd,pswdtxt,cpswdtxt;
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        curpswd=(EditText)findViewById(R.id.curpswd);
        pswdtxt=(EditText)findViewById(R.id.pswdtxt);
        cpswdtxt=(EditText)findViewById(R.id.cpswdtxt);
        submit=(Button)findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String curpswdStr = curpswd.getText().toString();
                String passwordStr = pswdtxt.getText().toString();
                String cpasswordStr = cpswdtxt.getText().toString();

             /*   linearLayout1 = (LinearLayout) findViewById(R.id.activity_item_details);

                LayoutInflater layoutInflater = (LayoutInflater) LoginPage.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View customView = layoutInflater.inflate(R.layout.popup_otp,null);

                //instantiate popup window
                popupWindow = new PopupWindow(customView, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);

                //display the popup window
                popupWindow.showAtLocation(linearLayout1, Gravity.NO_GRAVITY, 0, 0);
*/
             if (!passwordStr.equals(cpasswordStr)){
                 Toast.makeText(getBaseContext(), "Password and confirm Password Doesn't match", Toast.LENGTH_LONG).show();
             }else{
                 Log.d("jgytydtersxdfc",""+curpswdStr+""+passwordStr);
                 new HttpAsyncTask().execute(StaticInfo.getchangepswdUrl(StaticInfo.userId, curpswdStr, passwordStr));
             }
//                new HttpAsyncTask().execute(StaticInfo.getLoginUrl(mobileStr, passwordStr, androidDeviceId));
            }
        });
    }

    /*** Web Servcice Client  ***/

    public static String GET(String url){
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        return result;
    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }


    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            return GET(urls[0]);
        }
        @Override
        protected void onPostExecute(String result) {
            Log.d("dfghjklsh",result);
//            Toast.makeText(getBaseContext(), "Received:"+result, Toast.LENGTH_LONG).show();
            try {

//                Toast.makeText(getBaseContext(), "Received:"+result, Toast.LENGTH_LONG).show();

                if(result.equals("0")){
                    Toast.makeText(getBaseContext(), "Invalid ", Toast.LENGTH_LONG).show();
//                    popupWindow.dismiss();
                }else if(result.equals("success")){
//                    result=result.replace("success::","");
                    Toast.makeText(getBaseContext(), "Password Changed successfully", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(ChangePassword.this, DashboardActivity.class);
                    startActivity(intent);

                }else if(result.equals("failure")){
//                    result=result.replace("success::","");
                    Toast.makeText(getBaseContext(), "Current Password Invalid", Toast.LENGTH_LONG).show();
//                    Intent intent = new Intent(LoginPage.this, MainActivity.class);
//                    startActivity(intent);

                }else{
                    Toast.makeText(getBaseContext(), "Connection Failure", Toast.LENGTH_LONG).show();
//                    popupWindow.dismiss();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }







}