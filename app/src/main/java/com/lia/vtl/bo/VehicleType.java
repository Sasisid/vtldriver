package com.lia.vtl.bo;

public class VehicleType {
    private int vId;
    private String vtype;

    public  VehicleType(int vId,String vtype){
        this.vId=vId;
        this.vtype=vtype;
    }

    public int getvId() {
        return vId;
    }

    public void setvId(int vId) {
        this.vId = vId;
    }

    public String getVtype() {
        return vtype;
    }

    public void setVtype(String vtype) {
        this.vtype = vtype;
    }
}
